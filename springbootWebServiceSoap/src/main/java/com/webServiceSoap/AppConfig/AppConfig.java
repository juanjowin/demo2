package com.webServiceSoap.AppConfig;


import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.MessageDispatcher;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import com.webServiceSoap.dto.BillingDto;
import com.webServiceSoap.endpoint.BillignEndpoint;


@EnableWs
@Configuration
public class AppConfig extends WsConfigurerAdapter {
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext appcontext) {
		MessageDispatcherServlet servlet= new MessageDispatcherServlet();
		servlet.setApplicationContext(appcontext);
		servlet.setTransformWsdlLocations(true);
			
		return new ServletRegistrationBean(servlet, "/wsSoap/*");
	}
	
	@Bean(name="invoice")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema schema) {
		DefaultWsdl11Definition wsdl11Definition= new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("BillingDtoPort");
		wsdl11Definition.setLocationUri("/wsSoap");
		wsdl11Definition.setTargetNamespace(BillignEndpoint.NAMESPACE_URI);
		wsdl11Definition.setSchema(schema);
		
		return wsdl11Definition;
		
	}
	
	 @Bean
	    public XsdSchema invoiceSchema(){
	        return new SimpleXsdSchema(new ClassPathResource("/xsd/invoice.xsd"));
	    }

}
