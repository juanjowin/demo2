package com.webServiceSoap.dao;

import java.util.List;

import com.webServiceSoap.dto.BillingDto;

public interface BillingDao {
	public boolean save(BillingDto billingDto);
	public boolean update(BillingDto billingDto);
	public boolean delete(String numinvoice);
	public BillingDto findByInvoiceNumber(String numinvoice);
	public List<BillingDto> findAll();
}
