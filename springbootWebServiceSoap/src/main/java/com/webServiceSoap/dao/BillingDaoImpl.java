package com.webServiceSoap.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.webServiceSoap.dto.BillingDto;

@Repository
public class BillingDaoImpl implements BillingDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public BillingDto findByInvoiceNumber(String numinvoice) {
		return jdbcTemplate.queryForObject("select * from invoice where numinvoice=?", new BillingDtolRowMapper(), numinvoice); 
	}
	
	@Override
	public boolean save(BillingDto billingDto) {
		return jdbcTemplate.update("insert into invoice(idclient,numinvoice,total,creationdate) values(?,?,?,?)",
				billingDto.getIdclient(),billingDto.getNuminvoice(),billingDto.getTotal(),billingDto.getCreationdate())==1;
	}
	
	@Override
	public boolean update(BillingDto billingDto) {
		return jdbcTemplate.update("update invoice set  idclient=?, numinvoice=?, total=?, creationdate=? where numinvoice=?",
				billingDto.getIdclient(),billingDto.getNuminvoice(),billingDto.getTotal(),billingDto.getCreationdate(),billingDto.getNuminvoice())==1;
	}

	@Override
	public boolean delete(String numinvoice) {
		 return jdbcTemplate.update("delete from invoice where numinvoice=?",numinvoice)==1;
		
	}

	@Override
	public List<BillingDto> findAll() {
		return jdbcTemplate.query("select * from invoice" , new BillingDtolRowMapper());
	}

	

}
