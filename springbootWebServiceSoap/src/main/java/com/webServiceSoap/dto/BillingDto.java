package com.webServiceSoap.dto;


import java.io.Serializable;
import java.time.LocalDateTime;





public class BillingDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int idinvocie;
	private int idclient;
	private String numinvoice;
	private double total;
	private LocalDateTime creationdate;
	
	
	
	public BillingDto() {
	}

	public BillingDto(int idclient, String numinvoice, double total) {
		this.idclient = idclient;
		this.numinvoice = numinvoice;
		this.total = total;
	}
	
	public int getIdinvocie() {
		return idinvocie;
	}
	public void setIdinvocie(int idinvocie) {
		this.idinvocie = idinvocie;
	}
	public int getIdclient() {
		return idclient;
	}
	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}
	public String getNuminvoice() {
		return numinvoice;
	}
	public void setNuminvoice(String numinvoice) {
		this.numinvoice = numinvoice;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public LocalDateTime getCreationdate() {
		return creationdate;
	}
	public void setCreationdate(LocalDateTime creationdate) {
		this.creationdate = creationdate;
	}

	@Override
	public String toString() {
		return "BillingDto [idinvocie=" + idinvocie + ", idclient=" + idclient + ", numinvoice=" + numinvoice
				+ ", total=" + total + ", creationdate=" + creationdate + "]";
	}
	
	
	
	
	
}
