package com.webServiceSoap.endpoint;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.webServiceSoap.dto.BillingDto;
import com.webServiceSoap.service.BillingService;
import com.webservice.soap_ws.DeleteInvoiceRequest;
import com.webservice.soap_ws.DeleteInvoiceResponse;
import com.webservice.soap_ws.GetAllInvoiceRequest;
import com.webservice.soap_ws.GetAllInvoiceResponse;
import com.webservice.soap_ws.GetInvoiceByNumberRequest;
import com.webservice.soap_ws.GetInvoiceByNumberResponse;
import com.webservice.soap_ws.InvoiceType;
import com.webservice.soap_ws.SaveInvoiceRequest;
import com.webservice.soap_ws.SaveInvoiceResponse;
import com.webservice.soap_ws.ServiceStatus;
import com.webservice.soap_ws.UpdateInvoiceRequest;
import com.webservice.soap_ws.UpdateInvoiceResponse;


@Endpoint
public class BillignEndpoint {

	public static final String NAMESPACE_URI ="http://www.webServiceSoap/invoice";
	
	@Autowired
	private BillingService billingService;

	
	public BillignEndpoint() {
		
	}

	@Autowired
	public BillignEndpoint(BillingService billingService) {
		this.billingService = billingService;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getInvoiceByNumberRequest")
	@ResponsePayload
	public 	GetInvoiceByNumberResponse getInvoiceByNumber(@RequestPayload GetInvoiceByNumberRequest request) {
			GetInvoiceByNumberResponse response=new GetInvoiceByNumberResponse();
			BillingDto invoiceDto= billingService.findByInvoiceNumber(request.getNuminvoice());
			InvoiceType invoiceType= new InvoiceType();
			BeanUtils.copyProperties(invoiceDto, invoiceType);
			response.setInvoiceType(invoiceType);
			
			return response;
	}
	
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllInvoiceRequest")
	@ResponsePayload
	public 	GetAllInvoiceResponse getAllInvoice (@RequestPayload GetAllInvoiceRequest request) {
			GetAllInvoiceResponse response=new GetAllInvoiceResponse();
			List<InvoiceType> invoiceTypelist= new ArrayList<InvoiceType>();
			List<BillingDto> biList= billingService.findAll();
			for (BillingDto item: biList) {
				InvoiceType invoiceType=new InvoiceType();
				BeanUtils.copyProperties(item, invoiceType);
				invoiceTypelist.add(invoiceType);
			}
			response.getInvoiceType().addAll(invoiceTypelist);
			
			return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveInvoiceRequest")
	@ResponsePayload
	public 	SaveInvoiceResponse saveInvoice(@RequestPayload SaveInvoiceRequest request) {
			SaveInvoiceResponse response=new SaveInvoiceResponse();
			InvoiceType invoiceType=new InvoiceType();
			ServiceStatus serviceStatus=new ServiceStatus();
			
			BillingDto billingDto= new BillingDto(
			request.getIdclient(),
			request.getNuminvoice(),
			request.getTotal());
			
			boolean billingDto2=billingService.save(billingDto);
			
			if (billingDto2 == false){
				serviceStatus.setStatusCode("ERROR");
				serviceStatus.setMessage("no se pudo insertar factura");
			} else {

				BeanUtils.copyProperties(billingDto2, invoiceType);
				serviceStatus.setStatusCode("EXITO");
				serviceStatus.setMessage("factura insertado correctamente");
			}

			response.setInvoiceType(invoiceType);
			response.setServiceStatus(serviceStatus);
			return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateInvoiceRequest")
	@ResponsePayload
	public 	UpdateInvoiceResponse updateInvoice (@RequestPayload UpdateInvoiceRequest request) {
			
			UpdateInvoiceResponse response=new UpdateInvoiceResponse();
			ServiceStatus serviceStatus=new ServiceStatus();
			
			BillingDto billingDtoBD=billingService.findByInvoiceNumber(request.getNuminvoice());
			
			if (billingDtoBD==null) {
				serviceStatus.setStatusCode("ERROR");
				serviceStatus.setMessage("factura " + request.getNuminvoice() + " no encontrada");
			} else {
				billingDtoBD.setIdclient(request.getIdclient());
				billingDtoBD.setNuminvoice(request.getNuminvoice());
				billingDtoBD.setTotal(request.getTotal());
			}
			
			boolean invoice=billingService.update(billingDtoBD);
			
								
			if (invoice == false) {
				serviceStatus.setStatusCode("ERROR");
				serviceStatus.setMessage("no se pudo modificar factura");
			} else {
				serviceStatus.setStatusCode("EXITO");
				serviceStatus.setMessage("factura se modifico correctamente");
			}	
			response.setServiceStatus(serviceStatus);
			return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteInvoiceRequest") 
	@ResponsePayload
	public DeleteInvoiceResponse deleteInvoice(@RequestPayload DeleteInvoiceRequest request) {
		   DeleteInvoiceResponse response=new DeleteInvoiceResponse();
		   ServiceStatus serviceStatus=new ServiceStatus();
		   
		   boolean invoice=billingService.delete(request.getNuminvoice());
		   
		   if (invoice==false) {
			   serviceStatus.setStatusCode("ERROR");
			   serviceStatus.setMessage("factura con numero " +request.getNuminvoice() +" no encontrada");
			} else {
				serviceStatus.setStatusCode("EXITOSO");
				serviceStatus.setMessage("factura numero " + request.getNuminvoice() + " eliminada con exito");
			}
		   
		   	response.setServiceStatus(serviceStatus);
		   	return response;
	}
}

























