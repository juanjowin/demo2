package com.webServiceSoap.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.webServiceSoap.dao.BillingDao;
import com.webServiceSoap.dto.BillingDto;

@Service
public class BillingServiceImpl implements BillingService{
	
	@Autowired
	private BillingDao billingDao;

	@Override
	public boolean save(BillingDto billingDto) {
		return billingDao.save(billingDto);	
	}

	@Override
	public boolean update(BillingDto billingDto) {
		return billingDao.update(billingDto);		
	}

	@Override
	public BillingDto findByInvoiceNumber(String numinvoice) {
		return billingDao.findByInvoiceNumber(numinvoice);
	}

	@Override
	public List<BillingDto> findAll() {
		return billingDao.findAll();
	}
	@Override
	public boolean delete(String numinvoice) {
		return billingDao.delete(numinvoice);	
	}

}
