package com.webServiceSoap;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.webServiceSoap.AppConfig.AppConfig;
import com.webServiceSoap.dto.BillingDto;
import com.webServiceSoap.service.BillingService;
import com.webservice.soap_ws.GetAllInvoiceRequest;
import com.webservice.soap_ws.GetAllInvoiceResponse;
import com.webservice.soap_ws.InvoiceType;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(AppConfig.class)
public class ServiceTest{

	@Autowired
	private BillingService billingService;
	private static final String NAMESPACE_URI = "http://www.webServiceSoap/invoice";
	

	@Test
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllInvoiceRequest")
	@ResponsePayload
	public 	GetAllInvoiceResponse getAllInvoice (@RequestPayload GetAllInvoiceRequest request) {
			GetAllInvoiceResponse response=new GetAllInvoiceResponse();
			List<InvoiceType> invoiceTypelist= new ArrayList<InvoiceType>();
			List<BillingDto> biList= billingService.findAll();
			for (BillingDto item: biList) {
				InvoiceType invoiceType=new InvoiceType();
				BeanUtils.copyProperties(item, invoiceType);
				invoiceTypelist.add(invoiceType);
			}
			response.getInvoiceType().addAll(invoiceTypelist);
				
			return response;	
		}
	

}
